from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User

class Plan(models.Model):
    name = models.CharField(max_length=30)
    day = models.IntegerField()
    # Using TimeField leads to error I can't search the solution for.
    # I have to use TextField as a work around. Don't worry, we will convert it to 12-hours format on display.
    start_time = models.TextField(null=False, blank=False)
    end_time = models.TextField(null=False, blank=False) 
    owner = models.ForeignKey(User, default=None, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
