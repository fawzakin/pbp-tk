from django import template

register = template.Library()

# Workaround for time input
@register.filter
def ampm(time):
    time_check = time[0:2]
    am = [ "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11" ]
    if time_check in am:
        time_out = time + " a.m."
    elif time_check == "00" :
        time_out = "12" + time[2:] + " a.m."
    elif time_check == "12":
        time_out = "12" + time[2:] + " p.m."
    else:
        temp = int(time[0:2]) - 13
        time_out = am[temp] + time[2:] + " p.m."
    return time_out
