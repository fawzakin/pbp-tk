from django.shortcuts import render
from .models import Plan
# from .forms import PlanForm 
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import HttpResponse
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.core import serializers
from django.views.generic import ListView, View

class show_plan(LoginRequiredMixin, ListView):
    login_url = '/login/'
    redirect_field_name = 'redirect_to'
    template_name = 'planner.html'
    # queryset = Plan.objects.order_by('start_time')
    # context_object_name = 'planner'

    # We override getter to separate data. This is to improve performance on rendering the page.
    def get_queryset(self):
        return Plan.objects.order_by('start_time').filter(owner=self.request.user)

    def get_context_data(self, **kwargs):
        value = super(show_plan, self).get_context_data(**kwargs)
        for x in range(0,7):
            value["plan" + str(x)] = Plan.objects.all().order_by('start_time').filter(day=x).filter(owner=self.request.user)
        return value

class make_plan(LoginRequiredMixin, View):
    login_url = '/login/'
    def  get(self, request):
        name1 = request.GET.get('name', None)
        day1 = request.GET.get('day', None)
        start1 = request.GET.get('start_time', None)
        end1 = request.GET.get('end_time', None) 

        obj = Plan.objects.create(
            name = name1,
            day = day1,
            start_time = start1,
            end_time = end1,
            owner = request.user,
        )
        temp = {'id':obj.id,'name':obj.name,'day':obj.day,'start_time':obj.start_time,'end_time':obj.end_time} 
        response = { 'plan' : temp }

        return JsonResponse(response)

class update_plan(LoginRequiredMixin,View):
    login_url = '/login/'
    def  get(self, request):
        name1 = request.GET.get('name', None)
        day1 = request.GET.get('day', None)
        start1 = request.GET.get('start_time', None)
        end1 = request.GET.get('end_time', None) 
        id1 = request.GET.get('id', None)

        obj = Plan.objects.get(id=id1)
        obj.name = name1
        obj.day = day1
        obj.start_time = start1
        obj.end_time = end1

        obj.save()
        
        temp = {'id':obj.id,'name':obj.name,'day':obj.day,'start_time':obj.start_time,'end_time':obj.end_time} 
        response = { 'plan' : temp }
        
        return JsonResponse(response)

@login_required(login_url="/login/")
def delete_plan(request):
    id1 = request.GET.get('id', None)
    Plan.objects.get(id=id1).delete()
    response = { 'deleted': True }
    return JsonResponse(response)
