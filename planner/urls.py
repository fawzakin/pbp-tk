from django.urls import path
from .views import show_plan, make_plan, update_plan, delete_plan

urlpatterns = [
    path('', show_plan.as_view(), name='show_plan'),
    path('add', make_plan.as_view(), name='make_plan'),
    path('update', update_plan.as_view(), name='update_plan'),
    path('delete', delete_plan, name='delete_plan'),
]
