# Guidelines 
1. Please use English when involving everything within this repository (commit message, code comments, etc.)
2. Only use lowercase name for every created file unless specified otherwise. 
3. Do NOT push to `master` branch. Only to the branch you create yourself. I will handle every branch merges. Also, you should preferably only pull from `master` branch. 
4. Try to make your module as minimal as possible. Note that small codebase doesn't mean minimal module. Try to make them as efficient as you do in SDA. 
5. Do NOT implement anything that would be privacy invasive towards the end-users (such as trackers, ads, and **ANY GOOGLE SERVICES** such as its analytic and API). 
6. If you need to use external library or API, make sure they are (free and) open source. This project is licensed as a FOSS project so it's preferable to avoid any kind of proprietary junk. 
7. ~~Please do not be a deadliner. I'd be very disappointed if you finished the modules after the designated deadlines. Your pain is the pain for all of us.~~ This guideline is removed because I disobey it myself. We really all share the same pain.
8. Always base your branch with the latest commit from the master. I've seen somebody creating their branch with older commit of master which takes me time on reupdating files.
9. If you think a certain file or folder is not important for the webapp (e.g. `.idea` directory from IntelliJ IDE). Delete them from your local repo or list them in `.gitignore`. I only use the terminal and neovim to do everything within this repo.

# Merging
To merge your branch with master, please create a pull request and let me know through the group chat (don't do direct message). I will also notify you about new commits of the master branch in the group. If there are new commits during your working session, create commit of it after you are done AND pull from master (`git pull origin master`). Always make sure your work is based on the latest commit of master branch before pushing.

# Todo 
I will seperate all of our tasks into three stages with each stages having their own deadline. But first, please do the prerequisite before doing the first stage.

## Prerequisite 
1. Pull the newest commit of `master` branch to your branch 
    ```
    cd /path/to/repo
    git checkout [your_name]
    git pull origin master 
    ``` 
2. Create a local python environment 
    ```
    python -m venv env
    ``` 
3. Source your local environment
    ```
    source ./env/bin/activate
    ```
    I'm not sure about windows but my friend tells me that this works within powershell
    ```
    .\env\bin\activate
    ```
4. Install required python programs
    ```
    pip install -r requirement.txt
    ```
For every other stages, please do step 1 before proceeding.

## Current Task

### Stage 1: Module Creation
Create your module as stated in the readme. I consider a single app to be a single module. If you need to use some stuff from other module, please colaburate with the module dev. Feel free to edit the readme if your module differs from my suggestion. 

For the looks, you only need to do basic css job to make sure your webpages don't look bland. We will focus on the UI/UX in the stage 3. To help simplify its process, you may decide on how our final UI/UX should be. If everyone else agrees, let others develop the basic look of the proposed UI/UX while other eye candies (e.g. animation) can be done at stage 3.

### Stage 2/3: Module Unification
I've now combined the two stages into one since module unification is as simple as adding a hambuger menu that links to each and every modules. This stage will be focused on finishing the UI/UX. You are all have done a great job implementing something for the users to better interact with our modules as well as other eye candies for beautifying our webapp. I may do the whole ricing myself as I deem fit but you can help me on designing the webapp if you are better at web designing.

Don't forget to make them as light as you can as heavy animation can make the web app practically useable. Just don't go overboard with eye candies like Ristek websites do.

TIPS: We should do this stage and the later stage like we do during the thursday group work so we can finish this and move on to the next stage in one day.

---
If you have any question or something that I need to change here, please mention so in the group chat.
