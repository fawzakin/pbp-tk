from . import views
from django.urls import path

app_name = 'login'

urlpatterns = [
    path('',  views.login_view),
    path('validate/',  views.validate),
    path('flutter_login/', views.login_flutter),
    path('flutter_signup/', views.signup_flutter),
    path('flutter_logout/', views.logout_flutter)
]
