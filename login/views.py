from django.http import response, request
import json
from django.contrib.auth.backends import UserModel
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import LoginForm
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

def login_view(request):
    if request.user.is_authenticated:
        messages.warning(request, 'You have to logout your account first!')
        return redirect("/home")
    else:
        if request.method == "POST":
            form = LoginForm(data=request.POST)
            if form.is_valid():
                user = form.get_user()
                login(request, user)
                return redirect("/home")
        else:
            form = LoginForm()

        return render(request, "login.html", {"form": form})

def validate(response):
    username1 = response.GET.get('username', None)
    password1 = response.GET.get('password', None)
    if (len(password1)<8 or len(username1)==0):
        return JsonResponse({"status":"fail"})
    else:
        return JsonResponse({"status":"ok"})

def logout_view(response):
    logout(response)
    return redirect("/home")

@csrf_exempt
def login_flutter(request):
    user_data = json.loads(request.body)
    username = user_data['username']
    password = user_data['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            # Redirect to a success page.
            return JsonResponse({
                "status": True,
                "id":request.user.id,
                "username": request.user.username,
                "message": "Successfully Logged In!"
            }, status=200)
        else:
            return JsonResponse({
                "status": False,
                "message": "Failed to Login, Account Disabled."
            }, status=401)

    else:
        return JsonResponse({
            "status": False,
            "message": "Failed to Login, check your email/password."
        }, status=401)

@csrf_exempt
def signup_flutter(request):
    if request.method == 'POST':
        data = json.loads(request.body)

        username = data["username"]
        email = data["email"]
        password1 = data["password1"]

        new_user = UserModel.objects.create_user(
            username = username,
            email = email,
            password = password1,
        )

        new_user.save()
        login(request, new_user)
        return JsonResponse({
            "status": "success",
            "id": request.user.id,
            "username": request.user.username,
            }, status=200)
    else:
        return JsonResponse({"status": "error"}, status=401)

@csrf_exempt
def logout_flutter(request):
    try:
        logout(request)
        return JsonResponse({
            "status": True,
            "message": "Successfully Logged out!"
        }, status=200)
    except:
        return JsonResponse({
            "status": False,
            "message": "Failed to Logout"
        }, status=401)
