from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User

class LoginForm(AuthenticationForm):
    username = forms.CharField(
        max_length=30,
        required=True,
        widget=forms.TextInput(
            attrs={"placeholder": "Username", "class": "form-control","id": "username","name": "username"}
        )
    )

    password = forms.CharField(
        label="Password",
        max_length=30,
        min_length=8,
        required=True,
        widget=forms.PasswordInput(
            attrs={"placeholder": "Password","class": "form-control","name": "password"}
        )
    )
    class Meta:
        model = User
        fields = ['username', 'password']
