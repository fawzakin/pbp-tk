$(document).ready(function(){
    if ($('#note').is(':empty')){
        $("#note").text("Click edit button to start typing your note");
        $("#note").addClass("comment");
    }

    $("#editBtn").click(function(){
        $("#view").hide();
        $("#form").show();
    })

    $("#saveBtn").click(function (e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/note/edit/',
            data: $("#noteForm").serialize(),
            success: function (response) {
                var instance = response["instance"];
                instance = instance.replace( /\\r?\\n/g, '<br />');
                var note = JSON.parse(instance)[0]["fields"]["note"];
                $("#note").html(note);
                if(!$('#note').is(':empty')) {
                    $("#note").removeClass("comment");
                } else {
                    $("#note").text("Click edit button to start typing your note");
                    $("#note").addClass("comment");
                }
            },
            error: function (response) {
                alert("Some error occured");
            }
        })
    })

    $("#saveBtn").click(function (){
        $("#view").show();
        $("#form").hide();
    })
});
