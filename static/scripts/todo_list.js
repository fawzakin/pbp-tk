$(document).ready(function() {
    var editedItem = null;
    //prevent re-submission on reload
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
    //get CSRF token
    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
      }
      const csrftoken = getCookie('csrftoken');
    //generate todo list
    $.ajax({
        url: '/todo/json/',
        type: 'GET',
        dataType: 'json',
    }).done(function(response) {
        for (var i in response.todos) {
            var todo = `<span class="task-span">${response.todos[i].task}</span>`
            var todo_desc = `<span>${response.todos[i].description}</span>`
            if(response.todos[i].done) {
                var item = `
                <div class="card">
                    <div class="card-body">
                        <input type="checkbox" class="checkbox" id="${response.todos[i].task}" name="${response.todos[i].task}" value="Done"> 
                        <label for="${response.todos[i].task}"> ${todo}</label>
                        <br>
                        <div class="description">${todo_desc}</div>
                        <div class="card-buttons"> 
                            <button type="button" class="btn button-main edit" id="edit" data-toggle="modal" data-target="#form-modal-edit">Edit</button>
                            <button class="btn button-main del" id="delete" type="button">Delete</button> 
                        </div> 
                    </div>
                </div>`
                $('#todo-container').append(item)
            } else {
                var item = `
                <div class="card">
                    <div class="card-body">
                        <input type="checkbox" class="checkbox" id="${response.todos[i].task}" name="${response.todos[i].task}" value="Done" checked> 
                        <label for="${response.todos[i].task}">${todo}</label>
                        <br>
                        <div class="description">${todo_desc}</div>
                        <div class="card-buttons"> 
                            <button type="button" class="btn button-main edit" id="edit" data-toggle="modal" data-target="#form-modal-edit">Edit</button>
                            <button class="btn button-main del" id="delete" type="button">Delete</button> 
                        </div> 
                    </div>
                </div>`
                $('#todo-container').append(item)
            }
        }
    })
    //handling add task popup
    $('#add-task').click(function() {
        $('#form-modal-add').modal('show')
    })
    $('#add-task-bottom').click(function() {
        $('#form-modal-add').modal('show')
    })
    $('#close-popup-add').click(function() {
        $('#form-modal-add').modal('hide')
    })
    $('#close-popup-edit').click(function() {
        $('#form-modal-edit').modal('hide')
    })
    $('#cancel-button').click(function(e) {
        e.preventDefault();
        $('#form-modal-edit').modal('hide')
    })
    //add task to todo list
    $('#submit-button').click(function(e) {
        e.preventDefault();
        if (!$('#task').val().trim()) {
            alert('Please fill the task name.')
            $('#task').val("")
            $('#description').val("")
        } else {
            var data = {
                task: $('#task').val(),
                description: $('#description').val(),
                csrfmiddlewaretoken: csrftoken
            }
            $.ajax({
                url: "/todo/add/",
                type: 'GET',
                data: data,
            }).done(function(response) {
                if (response.status == "created") {
                    var temp = `
                    <div class="card">
                        <div class="card-body">
                            <input type="checkbox" class="checkbox" id="${response.task}" name="${response.task}" value="Done"> 
                            <label for="${response.task}"> <span class="task-span">${response.task}</span></label>
                            <br>
                            <div class="description">${response.description}</div>
                            <div class="card-buttons"> 
                                <button type="button" class="btn button-main edit" id="edit" data-toggle="modal" data-target="#form-modal-edit">Edit</button>
                                <button class="btn button-main del" id="delete" type="button">Delete</button> 
                            </div> 
                        </div>
                    </div>`
                $('#todo-container').append(temp)
                $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
                } else if (response.status == "error") {
                    alert('Task name must be unique.')
                }
            })
            $('#task').val("")
            $('#description').val("")
            $('#close-popup-add').trigger('click')  
        }
    })
    //Mark task as done / not done
    $('#todo-container').on('click', '.card', function() {
        var todo_task = $(this).children().children().children().first().text()
        var todo_done = document.getElementById(todo_task).checked
        if (todo_done == false) {
            done = 1
        } else {
            done = 0
        }
        $.ajax({
            url : "/todo/check/",
            type : 'POST',
            data : {
                task : todo_task,
                done : done,
                csrfmiddlewaretoken : csrftoken
            },
        }).done(function(response) {
              if (response.status == 'checked' & response.done) {
                document.getElementById(todo_task).checked = false
              } else if (response.status === 'checked' & !response.done) {
                document.getElementById(todo_task).checked = true
              }
        })
    //Edit a task
    }).on('click', '#edit', function(e) {
        e.stopPropagation();
        var todo_tag = $(this).parent().parent().parent();
        editedItem = todo_tag
        $('#edit-task').val(editedItem.children().children().children().first().text())
        $('#edit-description').val(editedItem.children().children().children().eq(1).text())
        $('#form-modal-edit').modal('show')
        $('#save-button').click(function(e) {
            e.preventDefault();
            if (!$('#edit-task').val().trim()) {

            } else {
                var data = {
                    task: editedItem.children().children().children().first().text(),
                    new_task: $('#edit-task').val(),
                    new_description: $('#edit-description').val(),
                    csrfmiddlewaretoken: csrftoken
                }
                $.ajax({
                    url: "/todo/edit/",
                    type: 'GET',
                    data: data,
                }).done(function(response) {
                    if (response.status == "edited") {
                        editedItem.children().children().children().first().text(response.task)
                        editedItem.children().children().children().eq(1).text(response.description)
                        editedItem = null;
                    } else if (response.status == "error") {
                        alert('Task name must be unique.')
                    }
                })
                $('#edit-task').val("")
                $('#edit-description').val("")
                $('#close-popup-edit').trigger('click')  
            }
        })
    //Delete task from todo list
    }).on('click', '#delete', function(e) {
        e.stopPropagation();
        var todo_tag = $(this).parent().parent().parent();
        var todo_task = todo_tag.children().children().children().first().text()
        if (!document.getElementById(todo_task).checked) {
            let confirmation = confirm("You haven't finished this task yet. Are you sure you want to delete it?")
            if (confirmation) {
                $.ajax({
                    url: "/todo/delete/",
                    type: 'POST',
                    data: {
                        task: todo_task,
                        csrfmiddlewaretoken: csrftoken
                    },
                }).done(function(response) {
                    if (response.status === 'deleted') {
                        todo_tag.remove();
                    }
                })
            }
        } else {
            $.ajax({
                url: "/todo/delete/",
                type: 'POST',
                data: {
                    task: todo_task,
                    csrfmiddlewaretoken: csrftoken
                },
            }).done(function(response) {
                if (response.status === 'deleted') {
                    todo_tag.remove();
                }
            })
        }
    })
})
