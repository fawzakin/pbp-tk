// instantly reload after closing modal
// $('#editModal').on('hidden.bs.modal', function () {
//     location.reload();
// });

// If edit button clicked, copy data to form
$(".btnToEdit").click(function() {
    var btn = $(this);
    var str = btn.attr('id');
    var id = str.slice(4);

    if (id) {
        div_id = "#deadlUpdate-" + id;

        title = $(div_id).find(".itemTitle").attr('id');
        date = $(div_id).find(".itemDate").attr('id');
        description = $(div_id).find(".itemDescription").attr('id');

        $('#Utitle').val(title);
        $('#Udate').val(date);
        $('textarea#Udescription').val(description);
        $('.gimmeId').prop('id',id);
    }
});

// Update deadline data
$("form#updateThis").submit(function() {
    var title1 = $('input[name="Utitle"]').val().trim();
    var date1 = $('input[name="Udate"]').val().trim();
    var description1 = $('textarea[name="Udescription"]').val().trim();
    var id1 = $('.gimmeId').attr('id');

    if (title1 && date1 && description1 && id1) {
        // Create Ajax Call
        $.ajax({
            url: '/deadline/update',
            data: {
                'id': id1,
                'title': title1,
                'date': date1,
                'description': description1,
            },
            dataType: 'json',
            success: function (data) {
                if (data.deadline) {
                    location.reload(true);
                }
            }, error: function (){
                alert("Error Here");
            }
        });
    } else {
        alert("Something went wrong :(");
    }
    $('#updateThis').trigger("reset");
    $('#closeModal').trigger("click");
    return false;
});

// If delete button being clicked
$(".btnToDelete").click(function() {
    var btn = $(this);
    var str = btn.attr('id');
    str = str.slice(6);
    var action = confirm("Delete this Deadline?");
if (action != false) {
    $.ajax({
        url: '/deadline/cancel',
        data: {
            'id': str,
        },
        dataType: 'json',
        success: function (data) {
            if (data.deleted) {
                $("#upSection #deadl-" + str).remove();
                $("#doSection #deadl-" + str).remove();
            }
        }
    });
}
});

$("form#addNew").submit(function() {
    var title1 = $('input[name="title"]').val().trim();
    var date1 = $('input[name="date"]').val().trim();
    var description1 = $('textarea[name="description"]').val().trim();

    if (title1 && date1 && description1) {
        // Create Ajax Call
        $.ajax({
            url: '/deadline/create',
            data: {
                'title': title1,
                'date': date1,
                'description': description1,
            },
            dataType: 'json',
            success: function (data) {
                if (data.deadline) {
                    appendDiv(data.deadline);
                }
            }, error: function (data){
                alert("Error Here");
            }
        });
    } else {
        alert("Something went wrong :(");
    }
    $('form#addNew').trigger("reset");
    $('#closeModal').trigger("click");
    return false;
});

function appendDiv(data) {
    const monthName = ["Jan. ", "Feb. ", "March ", "April ", "May ", "June ", "July ", "Aug. ", "Sept. ", "Oct. ", "Nov. ", "Dec. "];
    theDate = data.date;
    dday = theDate.slice(8,10);
    dmonth = theDate.slice(5,7);
    dyear = theDate.slice(0,4);
    strMonth = "";
    for(let x=0; x<monthName.length; x++) {
        if(x+1===Number(dmonth)){
            strMonth = monthName[x];
            break;
        }
    }
    theTime = theDate.slice(11);
    finalStr = strMonth+dday+", "+dyear+" ["+theTime+"] ";
    var today = new Date();
    var dd = parseInt(String(today.getDate()).padStart(2, '0'));
    var mm = parseInt(String(today.getMonth() + 1).padStart(2, '0')); //January is 0!
    var yyyy = parseInt(today.getFullYear());
    var todayVal = dd + mm*31 + yyyy*366;
    var thisVal = parseInt(dday) + parseInt(dmonth)*31 + parseInt(dyear)*366;

    var hasilKurang = parseInt(thisVal-todayVal);
    
    var theSection = "";
    if(hasilKurang>=0){
        theSection = "#upSection";
    } else{
        theSection = "#doSection";
    }

    $(theSection).append(`
        <div class="deadl my-3" id="deadl-{{item.id}}">
          <div class="data" id="deadlUpdate-{{item.id}}">
            <h4 class="titleDeadl itemTitle" id="{{item.title}}"><b>${data.title}</b></h4>
            <hr style="height:2px;opacity:1;color:white;">
            <h5 class="itemDeadl itemDate" id="{{item.date}}"><b>Deadline:</b> ${finalStr}</h5>
            <h5 class="itemDeadl itemDescription" id="{{item.description}}"><b>Description:</b> ${data.description}</h5>
          </div>
          <p class="getOther-{{item.id}}"></p>
          <div class="triButton d-flex justify-content-evenly">
            <h5 class="fg-warn">Refresh Page!</h5>
          </div>
        </div>
    `);
}
