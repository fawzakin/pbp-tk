// If view button being clicked
$(".btnToView").click(function() {
    var btn = $(this);
    var str = btn.attr('id');
    str = str.slice(4);
    var datee = $("#appoUpdate-"+str).find(".itemDate").attr('id'); 
    var creator = $("#appoUpdate-"+str).find(".itemCreator").attr('id'); 

    // Create Ajax Call to post the ID
    $.ajax({
        url: '/appointment/postId',
        data: {
            'theID': str,
            'creator': creator
        },
        dataType: 'json',
        success: function (data) {
            if (data.postID) {
                var obj = data.postID[0];
                var namaa = data.creator.charAt(0) + data.creator.slice(1);
                console.log(namaa);
                var linknya = "";
                if(obj.link.length==1){
                    linknya = "-";
                } else if(obj.link.length<35){
                    linknya = obj.link;
                } else{
                    linknya = obj.link.slice(0,33) + "...";
                }
                // Begin appending
                if(obj.link.length == 1){

                    $('#table-view tr').remove();
                    $("#table-view").append(`
                    <tr id="viewtr">
                    <td id="info">Title</td>
                    <td id="info">: </td>
                    <td>${obj.title}</td>
                    </tr>
                    <tr id="viewtr">
                    <td id="info">Created by</td>
                    <td id="info">: </td>
                    <td>${namaa}</td>
                    </tr>
                    <tr id="viewtr">
                    <td id="info">Attendee</td>
                    <td id="info">: </td>
                    <td>${obj.people}</td>
                    </tr>
                    <tr id="viewtr">
                    <td id="info">Date</td>
                    <td id="info">: </td>
                    <td>${datee}</td>
                    </tr>
                    <tr id="viewtr">
                    <td id="info">Location</td>
                    <td id="info">: </td>
                    <td>${obj.location}</td>
                    </tr>
                    <tr id="viewtr">
                    <td id="info">Link</td>
                    <td id="info">: </td>
                    <td>${linknya}</td>
                    </tr>
                    <tr id="viewtr">
                    <td id="info">Descriptions</td>
                    <td id="info">: </td>
                    <td id="desc">${obj.description}</td>
                    </tr>
                    `);
                } else{

                    $('#table-view tr').remove();
                    $("#table-view").append(`
                    <tr id="viewtr">
                    <td id="info">Title</td>
                    <td id="info">: </td>
                    <td>${obj.title}</td>
                    </tr>
                    <tr id="viewtr">
                    <td id="info">Created by</td>
                    <td id="info">: </td>
                    <td>${namaa}</td>
                    </tr>
                    <tr id="viewtr">
                    <td id="info">Attendee</td>
                    <td id="info">: </td>
                    <td>${obj.people}</td>
                    </tr>
                    <tr id="viewtr">
                    <td id="info">Date</td>
                    <td id="info">: </td>
                    <td>${datee}</td>
                    </tr>
                    <tr id="viewtr">
                    <td id="info">Location</td>
                    <td id="info">: </td>
                    <td>${obj.location}</td>
                    </tr>
                    <tr id="viewtr">
                    <td id="info">Link</td>
                    <td id="info">: </td>
                    <td><a href="${obj.link}" target="blank">${linknya}<a></td>
                    </tr>
                    <tr id="viewtr">
                    <td id="info">Descriptions</td>
                    <td id="info">: </td>
                    <td id="desc">${obj.description}</td>
                    </tr>
                    `);
                }
                    
            }
        }, error: function (){
                alert("Error Getting the data");
            }
        });
})

// If cancel button being clicked
$(".btnToDelete").click(function() {
    var btn = $(this);
    var str = btn.attr('id');
    str = str.slice(6);
    var action = confirm("Cancel this appointment for every attendees?");
if (action != false) {
    $.ajax({
        url: '/appointment/cancel',
        data: {
            'id': str,
        },
        dataType: 'json',
        success: function (data) {
            if (data.deleted) {
            $("#appo-" + str).remove();
            }
        }
    });
}
});
// Create new apppointment
$("#addNew").submit(function() {
    var title1 = $('input[name="title"]').val().trim();
    var date1 = $('input[name="date"]').val().trim();
    var location1 = $('input[name="location"]').val().trim();
    var link1 = $('input[name="link"]').val().trim();
    var description1 = $('textarea[name="description"]').val().trim();
    var people1 = $('textarea[name="people"]').val().trim();

    if (title1 && date1 && location1 && link1 && description1) {

        // Create Ajax Call
        $.ajax({
            url: '/appointment/create',
            data: {
                'title': title1,
                'date': date1,
                'location': location1,
                'link': link1,
                'description': description1,
                'people': people1,
            },
            dataType: 'json',
            success: function (data) {
                if (data.appointment) {
                    appendDiv(data.appointment);
                }
            }, error: function (){
                alert("Error Here");
            }
        });
    } else {
        alert("Something went wrong :(");
    }
    $('#addNew').trigger("reset");
    $('#closeModal').trigger("click");
    return false;
});

// Method to append data to html via DOM
function appendDiv(data) {
    const monthName = ["Jan. ", "Feb. ", "March ", "April ", "May ", "June ", "July ", "Aug. ", "Sept. ", "Oct. ", "Nov. ", "Dec. "];
    theDate = data.date;
    dday = theDate.slice(8,10);
    dmonth = theDate.slice(5,7);
    dyear = theDate.slice(0,4);
    strMonth = "";
    for(let x=0; x<monthName.length; x++) {
        if(x+1===Number(dmonth)){
            strMonth = monthName[x];
            break;
        }
    }
    theTime = theDate.slice(11);
    finalStr = strMonth+dday+", "+dyear+", "+theTime;
    var today = new Date();
    var dd = parseInt(String(today.getDate()).padStart(2, '0'));
    var mm = parseInt(String(today.getMonth() + 1).padStart(2, '0')); //January is 0!
    var yyyy = parseInt(today.getFullYear());
    var todayVal = dd + mm*31 + yyyy*366;
    var thisVal = parseInt(dday) + parseInt(dmonth)*31 + parseInt(dyear)*366;

    var hasilKurang = parseInt(thisVal-todayVal);

    if(data.link.length<35){                                     // if link.length < 35
        var theSection = "";
        if(hasilKurang>=0 && hasilKurang<7){
            theSection = "#thSection";
        } else if(hasilKurang>=7){
            theSection = "#upSection";
        } else{
            theSection = "#doSection";
        }

        $(theSection).append(`
       <div class="appo my-3" id="appo-{{item.id}}">
         <!-- Inside each appointment -->
         <div class="data" id="appoUpdate-{{item.id}}">
          <h4 class="sub itemTitle" id="{{item.title}}">${data.title}</h4>
          <hr style="height:2px;opacity:1;color:white;">
          <h5 style="display:none" class="itemApo itemPeople" id="${data.people}">Attendees: ${data.people}</h5>
          <h5 class="itemApo itemDate" id="{{item.date}}">Date & Time: ${finalStr}</h5>
          <h5 style="display:none" class="itemApo itemLocationXD" id="${data.location}">Location: ${data.location}</h5>
          <h5 style="display:none" class="itemApo itemDescription" id="${data.description}">Description: ${data.description}</h5>
          <h5 class="itemApo itemLink" id="{{item.link}}">Link: <a href="${data.link}" target="blank">${data.link}</a></h5>
         </div>
         <p class="getOther-{{item.id}}" id="{{item.location}}%%%{{item.description}}"></p>
         <div class="triButton d-flex justify-content-evenly">
          <h5 class="fg-warn">Refresh Page!</h5>
        </div>
       </div>
        `);
    } else{                                                     // if link.length >= 35
        var newLink = data.link.slice(0,33) + "...";
        $(theSection).append(`
       <div class="appo my-3" id="appo-{{item.id}}">
         <!-- Inside each appointment -->
         <div class="data" id="appoUpdate-{{item.id}}">
          <h4 class="sub itemTitle" id="{{item.title}}"><b>${data.title}</b></h4>
          <hr style="height:2px;opacity:1;color:white;">
          <h5 style="display:none" class="itemApo itemPeople" id="${data.people}">Attendees: ${data.people}</h5>
          <h5 class="itemApo itemDate" id="{{item.date}}"><b>Date & Time:</b> ${finalStr}</h5>
          <h5 style="display:none" class="itemApo itemLocationXD" id="${data.location}">Location: ${data.location}</h5>
          <h5 style="display:none" class="itemApo itemDescription" id="${data.description}">Description: ${data.description}</h5>
          <h5 class="itemApo itemLink" id="{{item.link}}"><b>Link:</b> <a href="${newLink}" target="blank">${newLink}</a></h5>
         </div>
         <p class="getOther-{{item.id}}" id="{{item.location}}%%%{{item.description}}"></p>
         <div class="triButton d-flex justify-content-evenly">
          <h5 class="fg-warn">Refresh Page!</h5>
        </div>
       </div>
        `);
    } 
}

// If edit button clicked, copy data to form
$(".btnToEdit").click(function() {
    var btn = $(this);
    var str = btn.attr('id');
    var id = str.slice(4);

    if (id) {
        div_id = "#appoUpdate-" + id;

        title = $(div_id).find(".itemTitle").attr('id');        
        helper = $(".getOther-"+id).attr('id');
        helper1 = helper.split('%%%');
        
        locationx = helper1[0];
        link = $(div_id).find(".itemLink").attr('id');
        description = helper1[1];
        people = $(div_id).find(".itemPeople").attr('id');

        $('#Utitle').val(title);
        $('#Ulocation').val(locationx);
        $('#Ulink').val(link);
        $('textarea#Udesc').val(description);
        $('.gimmeId').prop('id',id);
        $('.gimmeId').prop('id',id);
        $('textarea#Upeople').val(people);
    }
});

// Update appointment data
$("form#updateThis").submit(function() {
    var title1 = $('input[name="Utitle"]').val().trim();
    var date1 = $('input[name="Udate"]').val().trim();
    var location1 = $('input[name="Ulocation"]').val().trim();
    var link1 = $('input[name="Ulink"]').val().trim();
    var description1 = $('textarea[name="Udess"]').val().trim();
    var people1 = $('textarea[name="Upeople"]').val().trim();
    var id1 = $('.gimmeId').attr('id');
    if (title1 && date1 && location1 && link1 && description1 && id1) {

        // Create Ajax Call
        $.ajax({
            url: '/appointment/update',
            data: {
                'id': id1,
                'title': title1,
                'date': date1,
                'location': location1,
                'link': link1,
                'description': description1,
                'people': people1,
            },
            dataType: 'json',
            success: function (data) {
                if (data.appointment) {
                    location.reload();
                }
            }, error: function (){
                alert("Error Here");
            }
        });
    } else {
        alert("Something went wrong :(");
    }
    $('#updateThis').trigger("reset");
    $('#closeModal').trigger("click");
    return false;
});
