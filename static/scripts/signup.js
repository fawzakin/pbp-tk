$(document).ready(function(){
    $("#username").change(function (e) {
      var username = $(this).val();

      $.ajax({
        url: 'validate_username/',
        data: {
          'username': username
        },
        dataType: 'json',
        success: function (data) {
          if (data.is_taken) {
            alert("A user with this username already exists.");
          }
        }
      });
    });

    $("#email").change(function (e) {
      var email = $(this).val();

      $.ajax({
        url: 'validate_email/',
        data: {
          'email': email
        },
        dataType: 'json',
        success: function (data) {
          if (data.is_taken) {
            alert("This email has already been registered.");
          }
        }
      });
    });
});
