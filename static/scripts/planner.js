// Create new planner 
$("#addNew").submit(function() {
    var name1 = $('input[name="name"]').val().trim();
    var day1 = $('select[name="day"]').val().trim();
    var start1 = $('input[name="start_time"]').val().trim();
    var end1 = $('input[name="end_time"]').val().trim();
    
    if (name1 && day1 && start1 && end1) {

        // Create Ajax Call
        $.ajax({
            url: '/planner/add',
            data: {
                'name': name1,
                'day': day1,
                'start_time': start1,
                'end_time': end1,
            },
            dataType: 'json',
            success: function (data) {
                if (data.plan) {
                    appendDiv(data.plan);
                }
            }, error: function (){
                alert("Error Here");
            }
        });
    } else {
        alert("Something went wrong :(");
    }
    $('#addNew').trigger("reset");
    $('#closeModal').trigger("click");
    return false;
});

// Method to append data to html via DOM
function appendDiv(data) {
    var theSection = "error";
    var start_ampm = ampm(data.start_time);
    var end_ampm = ampm(data.end_time);
    switch (data.day) {
        case "0":
            theSection = "#monSection";
            break;
        case "1":
            theSection = "#tueSection";
            break;
        case "2":
            theSection = "#wedSection";
            break;
        case "3":
            theSection = "#thuSection";
            break;
        case "4":
            theSection = "#friSection";
            break;
        case "5":
            theSection = "#satSection";
            break;
        case "6":
            theSection = "#sunSection";
            break;
    }
    $(theSection).append(`
          <div class="plan infobox my-3" id="plan-{{item.id}}">
            <div class="data" style="padding-top:12px" id="planUpdate-{{item.id}}">
              <h4 class="sub itemName" id="{{item.name}}">${data.name}</h4>
              <hr class="padLine" style="height:2px;opacity:1;color:white;">
              <h5 class="normal itemDate" id="{{item.start_time}}%%%{{item.end_time}}%%%{{item.day}}">${start_ampm} - ${end_ampm}</h5>
            </div>
            <div class="triButton d-flex justify-content-evenly">
                <h5 class="fg-warn"> Refresh Page! </h5>
            </div>
          </div>
        `);
}


// If edit button clicked, copy data to form
$(".btnToEdit").click(function() {
    var btn = $(this);
    var str = btn.attr('id');
    var id = str.slice(4);
    console.log(id);

    if (id) {
        div_id = "#planUpdate-" + id;

        name1 = $(div_id).find(".itemName").attr('id');        
        date1= $(div_id).find(".itemDate").attr('id');        
        date2= date1.split('%%%');
        start1 = ampm(date2[0]);
        end1= ampm(date2[1]);
        day1= date2[2];
        
        $('#Uname').val(name1);
        $('#Uday').val(day1);
        $('#Ustart').val(start1);
        $('#Lstart').val(start1);
        $('#Uend').val(end1);
        $('#Lend').val(end1);
        $('.gimmeId').prop('id',id);
        $('.gimmeId').prop('id',id);
    }
});

// Update appointment data
$("form#updateThis").submit(function() {
    var name1 = $('input[name="Uname"]').val().trim();
    var day1 = $('select[name="Uday"]').val().trim();
    var start1 = $('input[name="Ustart"]').val().trim();
    var end1 = $('input[name="Uend"]').val().trim();
    var id1 = $('.gimmeId').attr('id');

    if (name1 && day1 && start1 && end1 && id1) {

        // Create Ajax Call
        $.ajax({
            url: '/planner/update',
            data: {
                'id': id1,
                'name': name1,
                'day': day1,
                'start_time': start1,
                'end_time': end1,
            },
            dataType: 'json',
            success: function (data) {
                if (data.plan) {
                    location.reload();
                }
            }, error: function (){
                alert("Error Here");
            }
        });
    } else {
        alert("Something went wrong :(");
    }
    $('#updateThis').trigger("reset");
    $('#closeModal').trigger("click");
    return false;
});

// If Delete button is being clicked
$(".btnToDelete").click(function() {
    var btn = $(this);
    var str = btn.attr('id');
    var id = str.slice(6);
    var theSection = "error";
    var curday = "-1";

    if (id) {
        div_id = "#planUpdate-" + id;
        date1= $(div_id).find(".itemDate").attr('id');        
        date2= date1.split('%%%');
        curday = date2[2];
    }

    switch (curday) {
        case "0":
            theSection = "#monSection";
            break;
        case "1":
            theSection = "#tueSection";
            break;
        case "2":
            theSection = "#wedSection";
            break;
        case "3":
            theSection = "#thuSection";
            break;
        case "4":
            theSection = "#friSection";
            break;
        case "5":
            theSection = "#satSection";
            break;
        case "6":
            theSection = "#sunSection";
            break;
        default:
            break;
    }

    var action = confirm("Delete this plan?");

if (action != false) {
    $.ajax({
        url: '/planner/delete',
        data: {
            'id': id,
        },
        dataType: 'json',
        success: function (data) {
            console.log(data.deleted);
            if (data.deleted) {
            console.log(theSection + " #plan-" + id);
            $(theSection + " #plan-" + id).remove();
            }
        }
    });
}
});

// Method to convert time to meridiem form
// Bascially the same as ampm.py but in Javascript
function ampm(time) {
    const am = [ "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11" ];
    var time_out = "";
    var temp = 0;
    if (am.includes(time.slice(0,2))) {
        time_out = time + " AM";
    } else {
        temp = parseInt(time.slice(0,2)) - 12;
        time_out = am[temp] + time.slice(2) + " PM";
    }
    return time_out
}
