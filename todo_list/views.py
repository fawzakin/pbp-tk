from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render
from .models import Todo
from .forms import TodoForm

# Create your views here.
@login_required(login_url="/login/")
def todo_list(request):
    return render(request, "todo_list.html")

@login_required(login_url="/login/")
def todo_json(request):
    try:
        todo = Todo.objects.filter(user = request.user).values()
        return JsonResponse({'todos': list(todo)})
    except Todo.DoesNotExist:
        return JsonResponse({'todos': list()})

@login_required(login_url="/login/")
def todo_add(request):
    todo_task = request.GET.get('task', None)
    todo_desc = request.GET.get('description', None)
    existing_todo = Todo.objects.filter(task = todo_task, user = request.user)
    if existing_todo.exists():
        return JsonResponse({'status' : 'error'})
    todo = Todo.objects.create(task = todo_task, description = todo_desc,
                               user = request.user, done=True)
    return JsonResponse({'task' : todo.task, 'description' : todo.description,
                 'status' : 'created'})

@login_required(login_url="/login/")
def todo_edit(request):
    todo_task = request.GET.get('task', None)
    new_todo_task = request.GET.get('new_task', None)
    new_todo_desc = request.GET.get('new_description', None)
    edited_todo = Todo.objects.get(task = todo_task, user = request.user)
    existing_todo = Todo.objects.filter(task = new_todo_task, user = request.user)
    if existing_todo.exists() and todo_task != new_todo_task:
        return JsonResponse({'status' : 'error'})
    edited_todo.task = new_todo_task
    edited_todo.description = new_todo_desc
    edited_todo.save()
    return JsonResponse({'task' : new_todo_task, 'description' : new_todo_desc,
                         'status' : 'edited'})

@login_required(login_url="/login/")
def todo_delete(request):
    if request.method == 'POST':
         todo_task = request.POST.get('task')
         Todo.objects.filter(task = todo_task, user = request.user).delete()
         return JsonResponse({'status': 'deleted'})

@login_required(login_url="/login/")
def todo_check(request):
    if request.method == 'POST':
        todo_task = request.POST.get('task')
        todo_done = request.POST.get('done')
        checked_todo = Todo.objects.get(task = todo_task, user = request.user)
        if todo_done == '0':
            checked_todo.done = False
            checked_todo.save()
            return JsonResponse({'task' : checked_todo.task,
                                 'done' : checked_todo.done, 'status' : 'checked'})
        elif todo_done == '1':
            checked_todo.done = True
            checked_todo.save()
            return JsonResponse({'task' : checked_todo.task,'done' : checked_todo.done,
                                 'status' : 'checked'})
        
