from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Todo(models.Model):
    task = models.CharField(max_length=30, unique = True)
    description = models.CharField(max_length=150, null = True, blank = True)
    done = models.BooleanField()
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                                null = True, blank = True)
    
