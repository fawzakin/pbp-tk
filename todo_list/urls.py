from django.urls import path
from .views import todo_list, todo_json, todo_add, todo_edit, todo_delete, todo_check

urlpatterns = [
    path('', todo_list, name="/"),
    path('json/', todo_json, name="todo-json"),
    path('add/', todo_add, name="todo-add"),
    path('edit/', todo_edit, name="todo-edit"),
    path('delete/', todo_delete, name="todo-delete"),
    path('check/', todo_check, name="todo-check"),
]
