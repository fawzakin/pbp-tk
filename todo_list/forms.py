from django.forms import ModelForm, CharField, TextInput
from .models import Todo

class TodoForm(ModelForm):
    class Meta:
        model = Todo
        exclude = ('done', 'user',)
        error_messages = {
            'required' : 'Please Type'
        }
        task_attrs = {
            'type' : 'text',
            'placeholder' : 'Insert Task',
            'class' : 'textfield',
            'id' : 'task',
        }
        desc_attrs = {
            'type' : 'text',
            'placeholder' : 'Insert Description',
            'class' : 'textfield',
            'id' : 'description',
        }
        task = CharField(label="Task", required=True, max_length=30,
                         widget=TextInput(attrs=task_attrs))
        description = CharField(label="Description", required=False, max_length=150,
                         widget=TextInput(attrs=desc_attrs))

