from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.views.generic import View
from django.views.generic import ListView
from django.shortcuts import redirect, render
from .models import Deadline
from .forms import DeadlineForm

class CrudView(LoginRequiredMixin,ListView):
    login_url = '/login/'
    redirect_field_name = 'redirect_to'
    queryset = Deadline.objects.order_by('date')
    model = Deadline
    template_name = 'deadline.html'
    context_object_name = 'deadlines'

class CreateDeadline(LoginRequiredMixin,View):
    def  get(self, request):
        title1 = request.GET.get('title', None)
        date1 = request.GET.get('date', None)
        description1 = request.GET.get('description', None)

        obj = Deadline.objects.create(
            title = title1,
            date = date1,
            description = description1,
            author = request.user,
        )

        deadl = {'id':obj.id,'title':obj.title,'date':obj.date,'description':obj.description}
        data = {
            'deadline': deadl
        }
        return JsonResponse(data)

@login_required(login_url="/login/")
def deleteDeadline(request):
    id1 = request.GET.get('id', None)
    Deadline.objects.get(id=id1).delete()
    data = {
        'deleted': True
    }
    return JsonResponse(data)

class UpdateDeadline(LoginRequiredMixin,View):
    login_url = '/login/'
    def  get(self, request):
        id1 = request.GET.get('id', None)
        title1 = request.GET.get('title', None)
        date1 = request.GET.get('date', None)
        description1 = request.GET.get('description', None)

        obj = Deadline.objects.get(id=id1)
        obj.title = title1
        obj.date = date1
        obj.description = description1
        obj.save()
        
        deadl = {'id':obj.id,'title':obj.title,'date':obj.date,'description':obj.description}
        data = {
            'deadline': deadl
        }
        return JsonResponse(data)