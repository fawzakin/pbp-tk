from django import forms
from deadline.models import Deadline

class DeadlineForm(forms.ModelForm):
    class Meta:
        model = Deadline
        fields = '__all__'