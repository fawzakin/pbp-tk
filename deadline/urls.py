from . import views
from django.urls import path

urlpatterns = [
    path('',  views.CrudView.as_view(), name='crud_ajax'),
    path('create/',  views.CreateDeadline.as_view(), name='createDeadline'),
    path('update/',  views.UpdateDeadline.as_view(), name='UpdateDeadline'),
    path('cancel/',  views.deleteDeadline, name='cancel'),     
]