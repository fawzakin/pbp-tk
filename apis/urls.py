from django.urls import path
from .views import *

urlpatterns = [
    path('appointment/', ListAppointment.as_view()),
    path('appointment/<int:pk>/',DetailAppointment.as_view()),
    
    path('deadline/', ListDeadline.as_view()),
    path('deadline/<int:pk>/',DetailDeadline.as_view()),
    
    path('planner/', ListPlanner.as_view()),
    path('planner/<int:pk>/',DetailPlanner.as_view()),
    
    path('quicknote/', ListQuicknote.as_view()),
    path('quicknote/<int:pk>/',DetailQuicknote.as_view()),
    
    path('todo/', ListTodo.as_view()),
    path('todo/<int:pk>/',DetailTodo.as_view()),
]