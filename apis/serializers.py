from rest_framework import serializers

from appointment import models as appointmentModels
from deadline import models as deadlineModels
from planner import models as plannerModels
from quicknote import models as quicknoteModels
from todo_list import models as todoModels
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        fields=(
            'id',
            'username'
        )
        model=User

class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        fields=(
            'id',
            'title',
            'date',
            'location',
            'link',
            'description',
            'people',
            'author'
        )
        model=appointmentModels.Appointment
        
class DeadlineSerializer(serializers.ModelSerializer):
    class Meta:
        fields=(
            'id',
            'title',
            'date',
            'description',
            'author'
        )
        model=deadlineModels.Deadline
        
class PlannerSerializer(serializers.ModelSerializer):
    class Meta:
        fields=(
            'id',
            'name',
            'day',
            'start_time',
            'end_time',
            'owner'
        )
        model=plannerModels.Plan
        
class QuicknoteSerializer(serializers.ModelSerializer):
    class Meta:
        fields=(
            'id',
            'note',
            'user'
        )
        model=quicknoteModels.QuickNote
        
class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        fields=(
            'id',
            'task',
            'description',
            'done',
            'user'
        )
        model=todoModels.Todo