from rest_framework import generics

from appointment import models as appointmentModels
from deadline import models as deadlineModels
from planner import models as plannerModels
from quicknote import models as quicknoteModels
from todo_list import models as todoModels
from .serializers import *

# Appointment APIs
class ListAppointment(generics.ListCreateAPIView):
    queryset = appointmentModels.Appointment.objects.all()
    serializer_class = AppointmentSerializer
    
class DetailAppointment(generics.RetrieveUpdateDestroyAPIView):
    queryset = appointmentModels.Appointment.objects.all()
    serializer_class = AppointmentSerializer
    
# Deadline APIs
class ListDeadline(generics.ListCreateAPIView):
    queryset = deadlineModels.Deadline.objects.all()
    serializer_class = DeadlineSerializer
    
class DetailDeadline(generics.RetrieveUpdateDestroyAPIView):
    queryset = deadlineModels.Deadline.objects.all()
    serializer_class = DeadlineSerializer
    
# Planner APIs
class ListPlanner(generics.ListCreateAPIView):
    queryset = plannerModels.Plan.objects.all()
    serializer_class = PlannerSerializer
    
class DetailPlanner(generics.RetrieveUpdateDestroyAPIView):
    queryset = plannerModels.Plan.objects.all()
    serializer_class = PlannerSerializer
    
# Quicknote APIs
class ListQuicknote(generics.ListCreateAPIView):
    queryset = quicknoteModels.QuickNote.objects.all()
    serializer_class = QuicknoteSerializer
    
class DetailQuicknote(generics.RetrieveUpdateDestroyAPIView):
    queryset = quicknoteModels.QuickNote.objects.all()
    serializer_class = QuicknoteSerializer

# Todo APIs
class ListTodo(generics.ListCreateAPIView):
    queryset = todoModels.Todo.objects.all()
    serializer_class = TodoSerializer
    
class DetailTodo(generics.RetrieveUpdateDestroyAPIView):
    queryset = todoModels.Todo.objects.all()
    serializer_class = TodoSerializer