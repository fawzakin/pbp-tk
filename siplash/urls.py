"""siplash URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, re_path, include
from django.contrib import admin
from homepage.views import index as index_homepage
from homepage.views import about as about_homepage
from homepage.views import user_info as user_homepage 
from login import views as vl
import quicknote.urls as qn
from todo_list import urls as tlu


urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', index_homepage, name='index'),
    path('about/', about_homepage, name='about'),
    path('user/', user_homepage, name='user_info'),
    path("signup/", include('signup.urls')),
    path("login/", include('login.urls')),
    path("logout/", vl.logout_view, name="logout"),
    path("appointment/", include('appointment.urls')),
    path('planner/', include('planner.urls')),
    path("deadline/", include('deadline.urls')),
    path("note/", include(qn)),
    path("todo/", include(tlu)),
    path('apis/', include('apis.urls')),
    re_path(r'^$', index_homepage, name='index')
]
