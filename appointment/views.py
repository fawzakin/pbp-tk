from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from .models import Appointment
from django.views.generic import View
from django.views.generic import ListView

class CrudView(LoginRequiredMixin,ListView):
    login_url = '/login/'
    redirect_field_name = 'redirect_to'
    queryset = Appointment.objects.order_by('date')
    template_name = 'appointment.html'
    context_object_name = 'appointments'

@login_required(login_url="/login/")
def postId(request):
    theID = request.GET.get('theID', None)
    creator = request.GET.get('creator', None)
    appo = list(Appointment.objects.filter(id=theID).values())
    data = {
        'postID' : appo,
        'creator' : creator
    }
    return JsonResponse(data)

class CreateAppo(LoginRequiredMixin,View):
    login_url = '/login/'
    def  get(self, request):
        title1 = request.GET.get('title', None)
        date1 = request.GET.get('date', None)
        location1 = request.GET.get('location', None)
        link1 = request.GET.get('link', None)
        description1 = request.GET.get('description', None)
        people1 = request.GET.get('people', None)

        obj = Appointment.objects.create(
            title = title1,
            date = date1,
            location = location1,
            link = link1,
            description = description1, 
            people = people1, 
            author = request.user,
        )

        appo = {'id':obj.id,'title':obj.title,'date':obj.date,'location':obj.location,'link':obj.link,'description':obj.description,'people':obj.people}
        data = {
            'appointment': appo
        }
        return JsonResponse(data)

class UpdateAppo(LoginRequiredMixin,View):
    login_url = '/login/'
    def  get(self, request):
        id1 = request.GET.get('id', None)
        title1 = request.GET.get('title', None)
        date1 = request.GET.get('date', None)
        location1 = request.GET.get('location', None)
        link1 = request.GET.get('link', None)
        description1 = request.GET.get('description', None)

        obj = Appointment.objects.get(id=id1)
        obj.title = title1
        obj.date = date1
        obj.location = location1
        obj.link = link1
        obj.description = description1
        obj.save()
        
        appo = {'id':obj.id,'title':obj.title,'date':obj.date,'location':obj.location,'link':obj.link,'description':obj.description}
        data = {
            'appointment': appo
        }
        return JsonResponse(data)

@login_required(login_url="/login/")
def deleteAppo(request):
    id1 = request.GET.get('id', None)
    Appointment.objects.get(id=id1).delete()
    data = {
        'deleted': True
    }
    return JsonResponse(data)
