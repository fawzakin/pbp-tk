from django import forms
from .models import Appointment

class AppoForm(forms.ModelForm):
    class Meta:
        model = Appointment
        fields = "__all__"
        # widgets = {
        #     'title' : forms.TextInput(attrs={'class':'form-control','type':'text','name':'title','placeholder':'title'}),
        #     'date' : forms.DateTimeInput(attrs={'type': 'datetime-local','type':'datetime-local','name':'date'}),
        #     'location' : forms.Textarea(attrs={'class':'form-control','type':'text','name':'location','placeholder':'location'}),
        #     'link' : forms.TextInput(attrs={'class':'form-control','type':'text','name':'link','placeholder':'link'}),
        #     'description' : forms.Textarea(attrs={'class':'form-control','type':'text','name':'description','placeholder':'description'}),
        # }
