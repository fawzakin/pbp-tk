from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create Appointment model
class Appointment(models.Model):
    title = models.CharField(max_length=30)
    date = models.DateTimeField(null=False, blank=False)
    location = models.TextField()
    link = models.URLField()
    description = models.TextField()
    people = models.TextField()
    author = models.ForeignKey(User, default=None, on_delete=models.CASCADE)

    def getPeople(self):
        my_string = str(self.people)
        return [x.strip() for x in my_string.split(',')]

    def __str__(self):
        return self.title

    def get_year(self):
        return(self.date.year)
    
    def get_month(self):
        return(self.date.month)

    def get_day(self):
        return(self.date.day)

    def today_year(self):
        return datetime.now().year

    def today_month(self):
        return datetime.now().month

    def today_day(self):
        return datetime.now().day
    
    def day_type(self):
        return type(self.date.day)

    def first(self):
        if self.date.day-datetime.now().day < 7:
            return True
        else:
            return False

    def hasilKurang(self):
        return (self.date.day + self.date.month*31 + self.date.year*366)-(datetime.now().day + datetime.now().month*31 + datetime.now().year*366)