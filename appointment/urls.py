from . import views
from django.urls import path

urlpatterns = [
    path('',  views.CrudView.as_view(), name='crud_ajax'),
    path('postId/',  views.postId, name='postId'),   
    path('create/',  views.CreateAppo.as_view(), name='createAppo'),
    path('update/',  views.UpdateAppo.as_view(), name='updateAppo'),
    path('cancel/',  views.deleteAppo, name='cancel'), 
]