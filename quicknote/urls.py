from django.urls import path
from .views import view_note, edit_note

urlpatterns = [
    path('', view_note, name='quicknote-index'),
    path('edit/', edit_note, name='quicknote-edit'),
]