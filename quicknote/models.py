from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class QuickNote(models.Model):
    note = models.TextField(default='', blank = True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null = True, blank = True)