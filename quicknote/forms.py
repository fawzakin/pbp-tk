from django import forms
from .models import QuickNote

class NoteForm(forms.ModelForm):
    class Meta:
        model = QuickNote
        fields = ["note"]

        widgets = {
            'note': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Type your note here.', 'rows': '18'}),
        }