from django.contrib.auth.decorators import login_required
from django.http.response import JsonResponse
from django.shortcuts import render
from django.core import serializers
from .models import QuickNote
from .forms import NoteForm

# Create your views here.
@login_required(login_url="/login/")
def view_note(request):
    try:
        note = QuickNote.objects.get(user = request.user)
    except QuickNote.DoesNotExist:
        QuickNote.objects.create(user = request.user)
        note = QuickNote.objects.get(user = request.user)
    form = NoteForm(instance = note)
    response = {'quicknote' : note, 'form': form}
    return render(request, 'quick_note.html', response)

@login_required(login_url="/login/")
def edit_note(request):
    if request.is_ajax and request.method == "POST":
        note = QuickNote.objects.get(user = request.user)
        form = NoteForm(request.POST, instance = note)
        if form.is_valid():
            instance = form.save()
            ser_instance = serializers.serialize('json', [instance, ])
            return JsonResponse({"instance": ser_instance}, status=200)
    return JsonResponse({"error": ""}, status=400)
