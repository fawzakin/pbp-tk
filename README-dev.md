![High IQ Graphic Design](https://gitlab.com/fawzakin/pbp-tk/raw/master/docs/siplash.png)

# Simple Planner for Staying at Home (SIPLASH)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)
[![pipeline status](https://gitlab.com/fawzakin/pbp-tk/badges/master/pipeline.svg)](https://gitlab.com/fawzakin/pbp-tk/-/commits/master)

**WARNING! This is the development version of SIPLASH and may contain breaking bug.**  
Link: https://siplash-dev.herokuapp.com

SIPLASH is a minimal web planner designed without all kinds of bloat and distraction other web planners provide. It is aimed for people who are working or studying from home due to COVID-19 restriction (which is an overused theme to use for any kind of college assignment hence the "Staying at Home" part). Working and/or studying at home can be a hard task to perform due to many distraction everyone would ever encounter at their home. This web app is created in hope to reduce those distraction and help its target audience to have a better time management. Minimalism and privacy should be the priority depending on the skill of our devs.

## Developers (Group F05):
| NAME                          | ID (NPM)   |
| ----------------------------- | ---------- |
| Cynthia Mutiara               | 2006522026 |
| Evan Aurelrius                | 2006595904 |
| Fawzan Fawzi Yusuf            | 2006595910 |
| Mochammad Iqbal               | 2006531056 |
| Muhammad Hilmi Ihsan Mumtaaz  | 2006523552 |
| Razita Afrina Nuriyan         | 2006529360 |
| Virdian Harun Prayoga         | 2006595766 |

## Implemented Modules:
The list of modules will use the following template. 

### Module Name
- Dev: The person who works on this module  
- Desc: Something to describe the name   
- Models: A Django object  
- Form: The form for user to input their data to  
- AJAX: Things that change dynamically when you do something on it
- Logout: Things the user can do while being logged out  
- Login: Things the user can do while being logged in  
- Difficulty: The expected difficulty for its implementation

## List of Modules:

### Sign Up
- Dev: Mochammad Iqbal
- Desc: A page for the end-user to create a new account
- Models: Django's Built-in User model
- Form: Username, Email, Password
- AJAX: Check if the username or email has been used. If they do, tell the user
- Logout: Present the user with a form to sign up
- Login: Tell the user to logout before they can create more account
- Difficulty: Not too hard
 
### Login
- Dev: Cynthia Mutiara
- Desc: A page for the user to log into their account
- Models: None, use User model from Sign Up
- Form: Username, Password
- AJAX: Check if the username and password are correct. If not, tell them
- Logout: Present the user with a form to log in 
- Login: Show their account info (name and email)
- Difficulty: Tough enough

### Planner
- Dev: Fawzan Fawzi Yusuf
- Desc: The board to create the plan for every week.
- Models: Plan
- Form: Name, Day, Start Time, End Time
- AJAX: A pop-up to add/edit plan and delete button that immediately deletes the plan
- Logout: Redirect to login page
- Login: Show add, edit, and delete plan
- Difficulty: Not as nightmarish as expected

### Deadline
- Dev: Muhammad Hilmi Ihsan Mumtaaz
- Desc: Show deadline of anything, sorted by their due time
- Models: Deadline
- Form: Title, Date, Description
- AJAX: A pop-up to add/edit deadline and delete button that immediately deletes the deadline
- Logout: Redirect to login page
- Login: Show add, edit, and delete deadline
- Difficulty: Manageable

### Appointment
- Dev: Evan Aurelrius
- Desc: Like deadline but shows on other users
- Models: Appointment
- Form: Title, Date, People (Other users), Location, URL Link, Description
- AJAX: A pop-up to add/edit appointment and cancel button that immediately deletes the appointment
- Logout: Redirect to login page
- Login: Show add, edit, and delete appointment
- Difficulty: The real hell

### Todo List
- Dev: Razita Afrina Nuriyan
- Desc: A todo list
- Models: Todo
- Form: Task (Name), Description
- AJAX: A pop-up to add a todo list, dynamic checklist, and delete button that immediately deletes an entry
- Logout: Redirect to login page
- Login: Show add todo check
- Difficulty: Quite simple but a little complicated

### Quick Note
- Dev: Virdian Harun Prayoga
- Desc: A quick note for the user to put any random ideas into
- Models: QuickNote
- Form: A single text field
- AJAX: Transform the text into an editable note when you click on a button and vice-versa
- Logout: Redirect to login page
- Login: Show change note
- Difficulty: Should be easy

## Target Audience (Persona):
This app is targeted mainly for these users:

### Students
- Goal:
    - To have orginized study schedule
    - To create a list of what to do for studying or assingment next
- Pain:
    - Have a hard time to focus on things
    - Have bunch of deadline

### Freelancers
- Goal:
    - To have a distraction-free planner program
    - To have a more efficient usage of their planner app
- Pain:
    - Time constraint
    - Cannot learn anything more complex

## Accessibility:
- Users without an account:
    - Can only interact with the homepage
    - Other pages will be accessible but unusable
- Users with an account:
    - Can use everything this web app has to offer
    - Interact with every page
- Admins:
    - Can see the list of all users and **delete** it
    - ~~Depending on the skill of the devs, the admins **SHOULD NOT** have access to the rest of the database (e.g. planner database) for enhanced privacy~~ Due to time complexity, we cannot implement encryption for database. We will state out privacy policy in the about page when the webapp is finished
 
# Guidelines and Todo (For Devs)
Please see the guidelines and todo list in TODO.md [here](docs/TODO.md).

# License and Credit
AGPLv3 License.

Used Libraries:
- Bootstrap Framework.
- JQuery.
 
 References:
 - [studygyaan.com (AJAX CRUD)]( https://studygyaan.com/django/how-to-execute-crud-using-django-ajax-and-json)
 
[Link to Assignment Doc](https://docs.google.com/document/d/14GU1G2GU52HO8H8mnPbmTNv7XpqYIDdc7vRsjyF8uH4)
