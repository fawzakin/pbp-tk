from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib import messages
from django.contrib.auth.models import User
from django.http import JsonResponse
from .forms import SignupForm


# Create your views here.
def signup(response):
    if response.user.is_authenticated:
        messages.warning(response, 'You have to logout your account first!')
        return redirect("/home")
    else:
        if response.method == "POST":
            form = SignupForm(response.POST)
            if form.is_valid():
                user = form.save()
                login(response, user)
                messages.success(response, f'Welcome to SIPLASH, { user.username }')
                return redirect("/home")

        else:
            form = SignupForm()

        return render(response, "signup/signup.html", {"form": form})


def validate_username(response):
    username = response.GET.get('username', None)
    data = {
        'is_taken': User.objects.filter(username=username).exists()
    }
    if data['is_taken']:
        data['error_message'] = 'A user with this username already exists.'
    return JsonResponse(data)


def validate_email(response):
    email = response.GET.get('email', None)
    data = {
        'is_taken': User.objects.filter(email=email).exists()
    }
    if data['is_taken']:
        data['error_message'] = 'This email has already been registered.'
    return JsonResponse(data)
