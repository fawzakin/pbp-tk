from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns = [
    path('', views.signup, name='login'),
    path('validate_username/', views.validate_username, name='validate_username'),
    path('validate_email/', views.validate_email, name='validate_email')
]
